#!/bin/bash

# $1 = RunUAT.sh fullpath
# $2 = Install full path, MUST END WITH .../Plugins/

$1 BuildPlugin -Plugin=`pwd`/EmacsSourceCodeAccess.uplugin -TargetPlatforms=Linux -Package=$2/Plugins/
